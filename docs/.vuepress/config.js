module.exports = {
    title: 'Nehemiah I. Dacres',
    heroText: 'Resume',
    description: 'Vuepress-powered resume hosted on GitLab Pages',
    base: '/',
    dest: 'public',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Photos', link: '/photos/' },
      { text: 'Blog', link: '/blog/' },
      { text: 'Vlog', link: '/vlog/' }
    ]
  }
}
