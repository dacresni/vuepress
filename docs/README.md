---
home: true
heroImage: /hero.png
layout: Layout
features:
- title: Server Side Technologies 
  details: I am most experiecned in Rails, Django but I've also leveraged Jupyter, Jekyll, and Flask for prototyping purposes.
- title: Client Side Technologies
  details: Vue is my prefered tool here aside from leveraging Boostrap's layout tech. I've typically used css and html directly.
- title: Special Concerns
  details: I'm comfortable with both ESRI and Bountless Geospacial tools.
footer: MIT Licensed | Copyright © 2018-present Evan You
---
